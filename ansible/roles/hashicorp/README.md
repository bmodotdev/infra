Role Name
=========

Install Hashicorp tools.

Requirements
------------

None.

Role Variables
--------------

### General Variables
| variable                    | required  | type    | default                                 | comments                    |
|-----------------------------|-----------|---------|-----------------------------------------|-----------------------------|
| hashicorp_install_packages  | no        | list    | `packer`,`terraform`,`vagrant`,`vault`  | Hashicorp tools to install  |

### Debian Variables
Generally speaking, these should not need any modification.
| variable                              | required  | type    | default                                             | comments                                  |
|---------------------------------------|-----------|---------|-----------------------------------------------------|-------------------------------------------|
| hashicorp_debian_asc_file             | no        | string  | `/usr/share/keyrings/hashicorp-archive-keyring.asc` | Path to install ASC key                   |
| hashicorp_debian_cache_time           | no        | int     | `3600`                                              | Valid cache time for package dependencies |
| hashicorp_debian_dep_packages         | no        | list    | `gpg`                                               | List of package dependencies              |
| hashicorp_debian_gpg_file             | no        | string  | `/usr/share/keyrings/hashicorp-archive-keyring.gpg` | Path to install GPG key                   |
| hashicorp_debian_gpg_fingerprint      | no        | string  | `798AEC654E5C15428C8E42EEAA16FCBCA621E701`          | Signing key fingerprint                   |
| hashicorp_debian_gpg_fingerprint_old  | no        | string  | `E8A032E094D8EB4EA189D270DA418C88A3219F7B`          | Old signing key fingerprint               |
| hashicorp_debian_gpg_url              | no        | string  | `https://apt.releases.hashicorp.com/gpg`            | URL to download the GPG key               |
| hashicorp_debian_repo_url             | no        | string  | `https://apt.releases.hashicorp.com`                | Apt repository URL                        |

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - hashicorp

License
-------

GPL v3
