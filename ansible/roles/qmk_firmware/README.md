QMK Firmware
=========

Install and configure QMK Firmware.


Requirements
------------

None.

Role Variables
--------------

| variable                    | required  | type    | default                             | comments                                          |
|-----------------------------|-----------|---------|-------------------------------------|---------------------------------------------------|
| qmk_firmware_home           | yes       | string  | undefined                           | The `qmk_home` confirugration value               |
| qmk_firmware_repo_origin    | yes       | string  | undefined                           | Your fork of qmk_firmware                         |
| qmk_firmware_repo_upstream  | no        | string  | git@github.com:qmk/qmk_firmware.git | The main upstream qmk_firmware repo               |
| qmk_firmware_udev_url       | no        | string  | `https://raw.githubusercontent.com/qmk/qmk_firmware/master/util/udev/50-qmk.rules`      |
| qmk_firmware_udev_path      | no        | string  | `/etc/udev/rules.d/`                | The directory to install udev rules               |

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: qmk_firmware
           qmk_firmware_home: "/home/user/git/qmk_firmware"
           qmk_fimware_repo_origin: "git@github.com/user/qmk_firmware.git"

License
-------
GPL v3
