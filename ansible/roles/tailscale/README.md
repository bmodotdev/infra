Role Name
=========

Install Tailscale.

Requirements
------------

None.

Role Variables
--------------

| variable                    | required  | type    | default                                             | comments                            |
|-----------------------------|-----------|---------|-----------------------------------------------------|-------------------------------------|
| tailscale_package_key_dest  | no        | string  | `/usr/share/keyrings/tailscale-archive-keyring.gpg` | Path to install package signing key |
| tailscale_package_key_mode  | no        | string  | `0644`                                              | Permissions of package signing key  |
| tailscale_package_url_base  | no        | string  | `https://pkgs.tailscale.com/stable`                 | Package URL base                    |
| tailscale_packages          | no        | list    | `tailscale`                                         | Tailscale package names to install  |
| tailscale_authkey           | no        | string  | undefined                                           | Tailscale auth key to join tailnet  |

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      vars:
        - tailscale_authkey: hunter2
      roles:
         - tailscale

License
-------
GPL v3
