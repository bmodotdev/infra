Role Name
=========

Install the 1Password Desktop and CLI clients

Requirements
------------

None.

Role Variables
--------------

### Debian OS Family Variables
See the official 1Password [documentation](https://support.1password.com/install-linux/#debian-or-ubuntu).
| variable                        | required  | type    | default                                                             | comments                              |
|---------------------------------|-----------|---------|---------------------------------------------------------------------|---------------------------------------|
| onepassword_debsig_dir_keyrings | no        | string  | `/usr/share/debsig/keyrings/`                                       | Directory to install package keyring  |
| onepassword_debsig_dir_policies | no        | string  | `/etc/debsig/policies/`                                             | Directory to install package policy   |
| onepassword_debsig_id           | no        | string  | `AC2D62742012EA22`                                                  | Package signature ID                  |
| onepassword_debsig_policy_url   | no        | string  | `https://downloads.1password.com/linux/debian/debsig/1password.pol` | Package policy download URL           |
| onepassword_package_key_file    | no        | string  | `/usr/share/keyrings/1password-archive-keyring.gpg`                 | File path to install package key to   |
| onepassword_package_key_url     | no        | string  | `https://downloads.1password.com/linux/keys/1password.asc`          | Package key download URL              |
| onepassword_packages            | no        | list    | `1password`,`1password-cli`                                         | List of packages to install           |
| onepassword_repo_url            | no        | string  | `https://downloads.1password.com/linux/debian/amd64`                | Package repository URL                |

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      roles:
         - onepassword

License
-------
GPL v3
