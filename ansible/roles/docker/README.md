Role Name
=========

**This role is currently only supported with Debian/Ubuntu based distributions**

This role installs `docker` and `docker compose` via repository per Docker’s documentation
([debian](https://docs.docker.com/engine/install/debian/), [ubuntu](https://docs.docker.com/engine/install/ubuntu/)).
It also has limited ability to configure `/etc/docker/daemon.json` by setting the docker log-driver.

Requirements
------------

None.

Tags
----

Tags are provided for each task in this role for selctive executing or skipping of specific tasks.

| Tag                   | Description                       |
|-----------------------|-----------------------------------|
| docker_uninstall      | Uninstall docker packages first   |
| docker_install_deps   | Install docker dependencies       |
| docker_install        | Install docker packages           |
| docker_configure      | Configure docker                  |
| docker_enable         | Enable docker service             |

Role Variables
--------------

| Variable                  | Required  | Default       | Comments                                  |
|---------------------------|-----------|---------------|-------------------------------------------|
| docker_config_dir         | no        | /etc/docker   | It’s not recommended you change this      |
| docker_daemon_config      | no        | daemon.json   | It’s not recommended you change this      |
| docker_daemon_log_driver  | no        | local         | See the official [docs](https://docs.docker.com/config/containers/logging/configure/) |

Dependencies
------------

None.

Example Playbook
----------------

To install docker, docker-compose, and configure the docker log-driver to jounrald:

    - hosts: servers
      vars:
        docker_daemon_log_driver: journald
      roles:
         - docker

License
-------

GNU GPLv3
