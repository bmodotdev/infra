gitlab-runner
=========

Install and register a gitlab-runner on Debian or RedHat systems.

Requirements
------------

None.

Role Variables
--------------

Variables closely align with names from the official
[gitlab runner documentation](https://docs.gitlab.com/runner/register/index.html);
read it for further details behind each variable provided here.

### General Variables
| variable                          | required  | default                   | comments                                      |
|-----------------------------------|-----------|---------------------------|-----------------------------------------------|
| gitlab_runner_access_level        | no        | ref_protected             | “ref_protected” or “not_protected”            |
| gitlab_runner_api_url             | yes       | undefined                 | The GitLab API URL                            |
| gitlab_runner_description         | no        | gitlab-runner by ansible  | The name of the runner to displayed in GitLab |
| gitlab_runner_docker_image        | no        | alpine:latest             | The default runner docker image               |
| gitlab_runner_executor            | no        | docker                    | The runner executor type                      |
| gitlab_runner_maximum_timeout     | no        | 3600                      | How long a job may run                        |
| gitlab_runner_registration_token  | yes       | undefined                 | The GitLab registration token                 |
| gitlab_runner_run_untagged        | no        | true                      | Whether to run untagged jobs                  |

### Internal Variables
| variable                          | required  | default                                                                               |
|-----------------------------------|-----------|---------------------------------------------------------------------------------------|
| gitlab_runner_install_url_deb     | no        | https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh   |
| gitlab_runner_install_url_rpm     | no        | https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh   |
| gitlab_runner_install_script_path | no        | /tmp/gitlab_runner_install.sh                                                         |
| gitlab_runner_package_name        | no        | gitlab-runner                                                                         |
| gitlab_runner_path                | no        | /usr/bin/gitlab-runner                                                                |

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      vars:
        gitlab_runner_api_url: https://gitlab.com
        gitlab_runner_registration_token: hunter2
      roles:
         - gitlab-runner

License
-------

GNU GPLv3
